namespace NamPOS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "Shop_ShopId", "dbo.Shops");
            AddColumn("dbo.AspNetUsers", "firstName", c => c.String());
            AddColumn("dbo.AspNetUsers", "lastName", c => c.String());
            AddColumn("dbo.AspNetUsers", "address", c => c.String());
            AddColumn("dbo.AspNetUsers", "Shop_ShopId1", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Shop_ShopId1");
            AddForeignKey("dbo.AspNetUsers", "Shop_ShopId", "dbo.Shops", "ShopId");
            AddForeignKey("dbo.AspNetUsers", "Shop_ShopId1", "dbo.Shops", "ShopId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Shop_ShopId1", "dbo.Shops");
            DropForeignKey("dbo.AspNetUsers", "Shop_ShopId", "dbo.Shops");
            DropIndex("dbo.AspNetUsers", new[] { "Shop_ShopId1" });
            DropColumn("dbo.AspNetUsers", "Shop_ShopId1");
            DropColumn("dbo.AspNetUsers", "address");
            DropColumn("dbo.AspNetUsers", "lastName");
            DropColumn("dbo.AspNetUsers", "firstName");
            AddForeignKey("dbo.AspNetUsers", "Shop_ShopId", "dbo.Shops", "ShopId");
        }
    }
}
