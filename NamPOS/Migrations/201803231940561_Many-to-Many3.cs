namespace NamPOS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManytoMany3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "Shop_ShopId", "dbo.Shops");
            DropForeignKey("dbo.ShopUser", "ShopID", "dbo.Shops");
            DropForeignKey("dbo.ShopUser", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetUsers", new[] { "Shop_ShopId" });
            DropIndex("dbo.ShopUser", new[] { "ShopID" });
            DropIndex("dbo.ShopUser", new[] { "UserID" });
            CreateTable(
                "dbo.ShopUsers",
                c => new
                    {
                        ShopId = c.Int(nullable: false),
                        Id = c.String(nullable: false, maxLength: 128),
                        IsAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.ShopId, t.Id })
                .ForeignKey("dbo.Shops", t => t.ShopId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.Id, cascadeDelete: true)
                .Index(t => t.ShopId)
                .Index(t => t.Id);
            
            DropColumn("dbo.AspNetUsers", "Shop_ShopId");
            DropTable("dbo.ShopUser");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ShopUser",
                c => new
                    {
                        ShopID = c.Int(nullable: false),
                        UserID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.ShopID, t.UserID });
            
            AddColumn("dbo.AspNetUsers", "Shop_ShopId", c => c.Int());
            DropForeignKey("dbo.ShopUsers", "Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShopUsers", "ShopId", "dbo.Shops");
            DropIndex("dbo.ShopUsers", new[] { "Id" });
            DropIndex("dbo.ShopUsers", new[] { "ShopId" });
            DropTable("dbo.ShopUsers");
            CreateIndex("dbo.ShopUser", "UserID");
            CreateIndex("dbo.ShopUser", "ShopID");
            CreateIndex("dbo.AspNetUsers", "Shop_ShopId");
            AddForeignKey("dbo.ShopUser", "UserID", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ShopUser", "ShopID", "dbo.Shops", "ShopId", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUsers", "Shop_ShopId", "dbo.Shops", "ShopId");
        }
    }
}
