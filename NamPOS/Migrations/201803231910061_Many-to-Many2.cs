namespace NamPOS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManytoMany2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Shops", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "Shop_ShopId1", "dbo.Shops");
            DropIndex("dbo.Shops", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "Shop_ShopId1" });
            CreateTable(
                "dbo.ShopUser",
                c => new
                    {
                        ShopID = c.Int(nullable: false),
                        UserID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.ShopID, t.UserID })
                .ForeignKey("dbo.Shops", t => t.ShopID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID, cascadeDelete: true)
                .Index(t => t.ShopID)
                .Index(t => t.UserID);
            
            DropColumn("dbo.Shops", "ApplicationUser_Id");
            DropColumn("dbo.AspNetUsers", "Shop_ShopId1");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Shop_ShopId1", c => c.Int());
            AddColumn("dbo.Shops", "ApplicationUser_Id", c => c.String(maxLength: 128));
            DropForeignKey("dbo.ShopUser", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShopUser", "ShopID", "dbo.Shops");
            DropIndex("dbo.ShopUser", new[] { "UserID" });
            DropIndex("dbo.ShopUser", new[] { "ShopID" });
            DropTable("dbo.ShopUser");
            CreateIndex("dbo.AspNetUsers", "Shop_ShopId1");
            CreateIndex("dbo.Shops", "ApplicationUser_Id");
            AddForeignKey("dbo.AspNetUsers", "Shop_ShopId1", "dbo.Shops", "ShopId");
            AddForeignKey("dbo.Shops", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
