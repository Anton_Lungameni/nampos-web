namespace NamPOS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManytoMany : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Shops", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Shops", "ApplicationUser_Id");
            AddForeignKey("dbo.Shops", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Shops", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Shops", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.Shops", "ApplicationUser_Id");
        }
    }
}
