﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NamPOS.Models
{
    public class Shop
    {
        [Key]
        public int ShopId { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public String Title { get; set; }
        [DataType(DataType.Text)]
        public String Slogan { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime CreatedOn { get; set; }
        [DataType(DataType.Text)]
        public String CreatedBy { get; set; }
        [DataType(DataType.Upload)]
        public byte[] Logo { get; set; }

        public virtual ICollection<ShopUser> ShopUsers { get; set; }
    }
}