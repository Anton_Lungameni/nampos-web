﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NamPOS.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string firstName { get; set; }
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string lastName { get; set; }
        [DataType(DataType.MultilineText)]
        public string address { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public virtual ICollection<ShopUser> ShopUsers { get; set; }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: true)
        {

        }


        public DbSet<Shop> Shops { get; set; }
        public DbSet<ShopUser> ShopUser { get; set; }
       

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

           
            modelBuilder.Entity<ShopUser>().HasKey(x => new
            {
                x.ShopId,
                x.Id
            });

            modelBuilder.Entity<ShopUser>()
                .HasRequired(x => x.Shop)
                .WithMany(x => x.ShopUsers)
                .HasForeignKey(x => x.ShopId);

            modelBuilder.Entity<ShopUser>()
                .HasRequired(x => x.User)
                .WithMany(x => x.ShopUsers)
                .HasForeignKey(x => x.Id);
            

        }
    }
}