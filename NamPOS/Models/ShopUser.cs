﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NamPOS.Models
{
    public class ShopUser
    {
        public int ShopId { get; set; }
        public string Id { get; set; }
        public bool IsAdmin { get; set; }

        public virtual Shop Shop { get; set; }
        [ForeignKey("Id")]
        public virtual ApplicationUser User { get; set; }
    }
}