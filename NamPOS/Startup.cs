﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NamPOS.Startup))]
namespace NamPOS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
